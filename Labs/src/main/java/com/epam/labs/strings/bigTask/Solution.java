package com.epam.labs.strings.bigTask;

import com.epam.labs.strings.bigTask.interfaces.ISentenceService;
import com.epam.labs.strings.bigTask.model.Sentence;
import com.epam.labs.strings.bigTask.model.Text;
import com.epam.labs.strings.bigTask.services.SentenceService;
import com.epam.labs.strings.bigTask.services.TextService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.List;

public class Solution {
    private static final Logger logger = LogManager.getLogger(Solution.class);
    public static void main(String[] args) {
        Text text = new Text();
        List<Sentence> sentenceList = text.getSentenceList();
        ISentenceService sentenceService = new SentenceService();
        Long mostCommonSentence = sentenceService.MaxSentCountWithSimilarWords_1(sentenceList);
        logger.info("Most common words usage counts " + mostCommonSentence + " times");

        logger.info("Sorted sentences by word count increasing");
        sentenceService.SortSentencesByWordsCount_2(sentenceList)
                .forEach(sentence ->  logger.info(sentence.getSentence()));

        List<String> uniqueWords = sentenceService.FindUniqueWordFromFirstSentenceInText_3(sentenceList);
        logger.info("Unique words in first sentence is ");
        uniqueWords.forEach(word -> logger.info(word));

    }
}
