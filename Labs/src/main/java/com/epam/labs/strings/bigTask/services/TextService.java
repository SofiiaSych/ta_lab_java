package com.epam.labs.strings.bigTask.services;

import com.epam.labs.strings.bigTask.model.Sentence;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public  class TextService {
    FileService fileService;

    public TextService(){
        fileService = new FileService();
    }

    public List<Sentence> GetAllSentences(){
        String text = fileService.ReadFile();
        List<Sentence> sentences = new ArrayList<>();
        BreakIterator iterator = BreakIterator.getSentenceInstance(Locale.US);
        iterator.setText(text);
        int start = iterator.first();
        for ( int end = iterator.next(); end != BreakIterator.DONE; start = end, end = iterator.next()) {
            sentences.add(new Sentence(text.substring(start, end)));
        }
        return sentences;
    }
}
