package com.epam.labs.strings.bigTask.model;

import com.epam.labs.strings.bigTask.Constants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class Sentence {
    private String sentence;
    private List<String> wordList;

    public Sentence(String sentence) {
        Constants constants = new Constants();
        this.sentence = sentence;
        List<String> wordsList = new ArrayList<>();
        //Pattern pattern = Pattern.compile(constants.REG_EX_WORD);
        String[] readWords = sentence.split("[\\p{Punct}\\s]+");
        for(String word : readWords) {
            wordsList.add(word);
        }
        this.wordList = Arrays.asList(readWords);
    }

    public String getSentence() {
        return sentence;
    }

    public List<String> getWordList() {
        return wordList;
    }
}
