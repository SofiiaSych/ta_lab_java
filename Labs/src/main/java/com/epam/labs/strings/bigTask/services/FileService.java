package com.epam.labs.strings.bigTask.services;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileService {
    public static String ReadFile(){
        StringBuilder builder = new StringBuilder();
        List<Character> characters = new ArrayList<>();
        String text = "";
        try(FileInputStream fin=new FileInputStream("D://Courses EPAM//Company history.txt"))
        {
            int i=-1;
            while((i=fin.read())!=-1){
                characters.add((char)i);
            }
            characters.forEach(el -> builder.append(el));
            text = builder.toString();
            text = text.replace("\n", "")
                    .replace("\r", "")
                    .replace("\t", "");
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
        return text;
    }
}
