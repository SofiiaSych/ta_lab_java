package com.epam.labs.strings.bigTask.services;

import com.epam.labs.strings.bigTask.Constants;
import com.epam.labs.strings.bigTask.interfaces.ISentenceService;
import com.epam.labs.strings.bigTask.model.Sentence;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class SentenceService implements ISentenceService {
    TextService textService;
    Constants constants;
    Sentence sentence;


    public SentenceService(){
        constants = new Constants();
        textService = new TextService();
    }

    public List<String> GetAllWords(){
        List<String> wordsList = new ArrayList<>();
        Pattern pattern = Pattern.compile(constants.REG_EX_WORD);
        String[] readWords = pattern.split(this.sentence.getSentence());
        for(String word : readWords) {
            wordsList.add(word);
        }
        return this.sentence.getWordList();
    }

    public Long CountNonUniqueWords(Sentence sentence){
        Map<String,Long> countWords = sentence.getWordList().stream()
                .collect(Collectors.groupingBy(String::toLowerCase, Collectors.counting()));

        Long max = null;

        for (Map.Entry<String, Long> item : countWords.entrySet()){
            if (max == null || item.getValue() > max){
                max = item.getValue();
            }
        }
        return max;
    }

    public Integer CountWords(Sentence sentence){
        return sentence.getWordList().size();
    }

    @Override
    public Long MaxSentCountWithSimilarWords_1(List<Sentence> sentences) {
        Map<Sentence, Long> sentenceCount = new HashMap<>();
        sentences.forEach(sentence -> sentenceCount.put(sentence, CountNonUniqueWords(sentence)));

        Long max = null;
        for (Map.Entry<Sentence, Long> item : sentenceCount.entrySet()){
            if (max == null || item.getValue() > max){
                max = item.getValue();
            }
        }
        return max;
    }

    @Override
    public List<Sentence> SortSentencesByWordsCount_2(List<Sentence> sentences) {
        Map<Sentence, Integer> sentenceWordCount = new HashMap<>();
        sentences.forEach(sentence -> sentenceWordCount.put(sentence, CountWords(sentence)));

        return sentenceWordCount
                .entrySet()
                .stream()
                .sorted(Comparator.comparing(Map.Entry::getValue))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    @Override
    public List<String> FindUniqueWordFromFirstSentenceInText_3(List<Sentence> sentences) {
        Sentence firstSentence = sentences.get(0);
        List<String> uniqueWords = new ArrayList<>();
        String uniqueWord = "";
        for (String word : firstSentence.getWordList()){
            uniqueWord = word;
            for(int i = 1; i < sentences.size() - 1; i++){
                Sentence chosenSentence = sentences.get(i);
                for(String chosenWord: chosenSentence.getWordList()){
                    if (word.equals(chosenWord)){
                        uniqueWord = "";
                    }
                }
            }
            if(uniqueWord!=""){
                uniqueWords.add(uniqueWord);
            }
        }
        return uniqueWords;
    }
}
