package com.epam.labs.strings.bigTask.interfaces;

import com.epam.labs.strings.bigTask.model.Sentence;

import java.util.List;

public interface ISentenceService {
    Long MaxSentCountWithSimilarWords_1(List<Sentence> sentences);
    List<Sentence> SortSentencesByWordsCount_2(List<Sentence> sentences);
    List<String> FindUniqueWordFromFirstSentenceInText_3(List<Sentence> sentences);

}
