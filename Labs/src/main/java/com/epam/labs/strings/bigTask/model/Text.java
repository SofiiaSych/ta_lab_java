package com.epam.labs.strings.bigTask.model;

import com.epam.labs.strings.bigTask.services.FileService;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Text {
    private String text;
    private List<Sentence> sentenceList;

    public String getText() {
        return text;
    }

    public List<Sentence> getSentenceList() {
        return sentenceList;
    }

    public Text() {
        FileService fileService = new FileService();
        this.text = fileService.ReadFile();
        List<Sentence> sentences = new ArrayList<>();
        BreakIterator iterator = BreakIterator.getSentenceInstance(Locale.US);
        iterator.setText(this.text);
        int start = iterator.first();
        for ( int end = iterator.next(); end != BreakIterator.DONE; start = end, end = iterator.next()) {
            sentences.add(new Sentence(this.text.substring(start, end).replace(".","")));
        }
        this.sentenceList = sentences;
    }
}
