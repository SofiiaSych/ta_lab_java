package com.epam.labs.oopAndGame.electricalAppliances.appliance;

import com.epam.labs.oopAndGame.electricalAppliances.service.ISwitchable;
import com.epam.labs.oopAndGame.electricalAppliances.service.IUseElectricity;
import com.epam.labs.oopAndGame.electricalAppliances.data.ApplianceSize;

public abstract class ElectricalAppliance implements ISwitchable, IUseElectricity {
    public int power;
    ApplianceSize size;
    public ElectricalAppliance(int power, ApplianceSize size){
        this.power = power;
        this.size = size;
    }
    public void useElectricity() {
        System.out.println("I'm using electricity.");
    }
    public void turnOn(){
        System.out.println("The appliance was turned on.");
    }
    public void turnOff(){
        System.out.println("The appliance was turned off.");
    }
}