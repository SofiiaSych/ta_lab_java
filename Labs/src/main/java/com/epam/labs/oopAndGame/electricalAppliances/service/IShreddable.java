package com.epam.labs.oopAndGame.electricalAppliances.service;

import com.epam.labs.oopAndGame.electricalAppliances.data.ShreddingLevel;

public interface IShreddable {
    void shred(ShreddingLevel targetLevel);
}
