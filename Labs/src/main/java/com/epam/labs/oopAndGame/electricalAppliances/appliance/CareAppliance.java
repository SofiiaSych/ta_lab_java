package com.epam.labs.oopAndGame.electricalAppliances.appliance;

import com.epam.labs.oopAndGame.electricalAppliances.service.IHeatable;
import com.epam.labs.oopAndGame.electricalAppliances.data.ApplianceSize;

public abstract class CareAppliance extends ElectricalAppliance implements IHeatable {
    protected int temperature;
    public CareAppliance(int power, ApplianceSize size, int temperature){
        super(power, size);
        this.temperature = temperature;
    }
    public int increaseTemperature(int temp) {
        return this.temperature += temp;
    }
    public int decreaseTemperature(int temp) {
        return this.temperature -= temp;
    }
}
