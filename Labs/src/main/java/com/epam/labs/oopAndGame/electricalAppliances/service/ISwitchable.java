package com.epam.labs.oopAndGame.electricalAppliances.service;

public interface ISwitchable {
    void turnOn();
    void turnOff();
}

