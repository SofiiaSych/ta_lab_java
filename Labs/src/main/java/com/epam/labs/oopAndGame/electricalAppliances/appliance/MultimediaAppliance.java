package com.epam.labs.oopAndGame.electricalAppliances.appliance;

import com.epam.labs.oopAndGame.electricalAppliances.service.IDisplayable;
import com.epam.labs.oopAndGame.electricalAppliances.data.ApplianceSize;

public abstract class MultimediaAppliance extends ElectricalAppliance implements IDisplayable {
    public MultimediaAppliance(int power, ApplianceSize size){
        super(power, size);
    }
    public void display() {
        System.out.println("I'm displaying.");
    }
}
