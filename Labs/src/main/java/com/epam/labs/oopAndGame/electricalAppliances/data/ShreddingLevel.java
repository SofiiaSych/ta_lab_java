package com.epam.labs.oopAndGame.electricalAppliances.data;

public enum ShreddingLevel {
    ROUGH,
    MEDIUM,
    SMALL
}
