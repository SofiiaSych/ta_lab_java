package com.epam.labs.oopAndGame.electricalAppliances.service;

public interface IDisplayable {
    void display();
}
