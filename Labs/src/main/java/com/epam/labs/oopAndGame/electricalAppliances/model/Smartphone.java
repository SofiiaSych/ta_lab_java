package com.epam.labs.oopAndGame.electricalAppliances.model;

import com.epam.labs.oopAndGame.electricalAppliances.appliance.MultimediaAppliance;
import com.epam.labs.oopAndGame.electricalAppliances.data.ApplianceSize;

public class Smartphone extends MultimediaAppliance {
    private int cameraResolution;
    public Smartphone(int power, ApplianceSize size, int cameraResolution){
        super(power, size);
        this.cameraResolution = cameraResolution;
    }

    public void TakeSelfie(){
        System.out.println("You're cute! Post it on instagram right now!))");
    }
}
