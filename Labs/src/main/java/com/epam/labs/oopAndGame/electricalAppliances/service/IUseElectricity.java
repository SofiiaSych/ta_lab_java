package com.epam.labs.oopAndGame.electricalAppliances.service;

public interface IUseElectricity {
    void useElectricity();
}
