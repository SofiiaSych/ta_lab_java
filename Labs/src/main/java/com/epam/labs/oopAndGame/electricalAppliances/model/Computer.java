package com.epam.labs.oopAndGame.electricalAppliances.model;

import com.epam.labs.oopAndGame.electricalAppliances.data.ApplianceSize;
import com.epam.labs.oopAndGame.electricalAppliances.appliance.MultimediaAppliance;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Computer extends MultimediaAppliance {
    private int cpuPower;
    public Computer(int power, ApplianceSize size, int cpuPower){
        super(power, size);
        this.cpuPower = cpuPower;
    }

    public long CalculateDaysTillSummer() throws Exception{
        long today = new Date().getTime();
        String firstDayOfSummer2020string = "01/06/2020";
        long firstDayOfSummer2020 = new SimpleDateFormat("dd/MM/yyyy").parse(firstDayOfSummer2020string).getTime();
        return TimeToDate(firstDayOfSummer2020 - today);
    }

    private long TimeToDate(long time){
        int millisecondsInSecond = 1000;
        int secondsInMinute = 60;
        int minutesInHour = 60;
        int hoursInDay = 24;
        return time / (millisecondsInSecond * secondsInMinute * minutesInHour * hoursInDay);
    }
}
