package com.epam.labs.oopAndGame.electricalAppliances.model;

import com.epam.labs.oopAndGame.electricalAppliances.data.ApplianceSize;
import com.epam.labs.oopAndGame.electricalAppliances.appliance.CareAppliance;

public class HairStyler extends CareAppliance {
    public HairStyler(int power, ApplianceSize size, int temperature){
        super(power, size, temperature);
    }
}
