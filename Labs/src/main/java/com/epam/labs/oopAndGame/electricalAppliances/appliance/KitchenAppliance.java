package com.epam.labs.oopAndGame.electricalAppliances.appliance;

import com.epam.labs.oopAndGame.electricalAppliances.data.ApplianceSize;

public abstract class KitchenAppliance extends ElectricalAppliance{
    public KitchenAppliance(int power, ApplianceSize size){
        super(power, size);
    }

}
