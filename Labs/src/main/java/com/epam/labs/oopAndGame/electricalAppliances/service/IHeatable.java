package com.epam.labs.oopAndGame.electricalAppliances.service;

public interface IHeatable {
    int increaseTemperature (int temp);
    int decreaseTemperature (int temp);
}

