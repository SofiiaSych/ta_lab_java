package com.epam.labs.oopAndGame.electricalAppliances.model;

import com.epam.labs.oopAndGame.electricalAppliances.data.ApplianceSize;
import com.epam.labs.oopAndGame.electricalAppliances.service.IHeatable;
import com.epam.labs.oopAndGame.electricalAppliances.appliance.KitchenAppliance;

public class CoffeeMachine extends KitchenAppliance implements IHeatable {
    protected int temperature;
    public CoffeeMachine(int power, ApplianceSize size, int temperature){
        super(power, size);
        this.temperature = temperature;
    }
    public int increaseTemperature(int temp) {
        return this.temperature += temp;
    }
    public int decreaseTemperature(int temp) {
        return this.temperature -= temp;
    }
}
