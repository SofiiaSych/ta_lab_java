package com.epam.labs.oopAndGame.electricalAppliances.model;

import com.epam.labs.oopAndGame.electricalAppliances.appliance.MultimediaAppliance;
import com.epam.labs.oopAndGame.electricalAppliances.data.ApplianceSize;

public class TV extends MultimediaAppliance {
    private int channels;
    public TV(int power, ApplianceSize size, int channels){
        super(power, size);
        this.channels = channels;
    }
    public void ShowDiscovery(){
        System.out.println("Now you see two elephants near the river...");
    }
}
