package com.epam.labs.oopAndGame.electricalAppliances.data;

public enum ApplianceSize {
    SMALL,
    MEDIUM,
    LARGE
}
