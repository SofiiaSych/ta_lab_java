package com.epam.labs.oopAndGame.electricalAppliances;

import com.epam.labs.oopAndGame.electricalAppliances.appliance.CareAppliance;
import com.epam.labs.oopAndGame.electricalAppliances.model.*;
import com.epam.labs.oopAndGame.electricalAppliances.data.ApplianceSize;
import com.epam.labs.oopAndGame.electricalAppliances.data.ShreddingLevel;

import java.util.Random;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args){
        int option = new Random().nextInt(3);
        switch (option){
            case 0:
                System.out.println("You are in bathroom");
                UseBathroomDevice();
                break;
            case 1:
                System.out.println("You are in kitchen");
                UseKitchenDevice();
                break;
            case 2:
                System.out.println("You are in living room");
                UseLivingRoomDevice();
                break;
            default:
                System.out.println("You're outside of house.");
        }
    }

    private static int Scanner(){
        Scanner scanner = new Scanner(System. in);
        return scanner.nextInt();
    }

    private static void UseKitchenDevice(){
        System.out.println("Enter number of appliance that you want to use");
        System.out.println("1. Blender\n2. Coffee  machine\n3. Electrical stove");
        int device = Scanner();
        switch (device){
            case 1:
                Blender blender = new Blender(700, ApplianceSize.SMALL);
                blender.shred(ShreddingLevel.MEDIUM);
                System.out.println("used "+ blender.power + " of power");
                break;
            case 2:
                CoffeeMachine coffeeMachine = new CoffeeMachine(1500, ApplianceSize.MEDIUM, 20);
                coffeeMachine.increaseTemperature(80);
                System.out.println("Coffee has been made");
                System.out.println("used "+ coffeeMachine.power + " of power");
                break;
            case 3:
                ElectricalStove stove = new ElectricalStove(5000, ApplianceSize.LARGE, 20);
                stove.increaseTemperature(100);
                System.out.println("Soup has been cooked");
                System.out.println("used "+ stove.power + " of power");
        }
    }

    private static void UseBathroomDevice(){
        System.out.println("Enter number of appliance that you want to use");
        System.out.println("1. Hair dryer\n2. Hair styler");
        int device = Scanner();
        switch (device) {
            case 1:
                CareAppliance hairDryer = new HairDryer(2000, ApplianceSize.SMALL, 20);
                hairDryer.increaseTemperature(20);
                System.out.println("Hair was dried." + "used " + hairDryer.power + " of power");
                break;
            case 2:
                CareAppliance hairStyler = new HairStyler(300, ApplianceSize.SMALL, 20);
                hairStyler.increaseTemperature(160);
                System.out.println("Hair was styled ." + "used " + hairStyler.power + " of power");
                break;
        }
    }

    private static void UseLivingRoomDevice(){
        System.out.println("Enter number of appliance that you want to use");
        System.out.println("1. Computer\n2. Smartphone\n3. TV");
        int device = Scanner();
        switch (device) {
            case 1:
                Computer pentium4 = new Computer(3000, ApplianceSize.MEDIUM, 2000);
                try {
                    long daysTillSummer = pentium4.CalculateDaysTillSummer();
                    System.out.println(daysTillSummer + "days left till summer");
                    System.out.println("used " + pentium4.power + " of power");
                }
                catch (Exception e){ System.out.println("Summer won't come."); }
                break;
            case 2:
                Smartphone pixel3 = new Smartphone(300, ApplianceSize.SMALL, 15);
                pixel3.TakeSelfie();
                System.out.println("used " + pixel3.power + " of power");
                break;
            case 3:
                TV rassvet = new TV(1000, ApplianceSize.LARGE, 5);
                rassvet.ShowDiscovery();
                System.out.println("used " + rassvet.power + " of power");
                break;
        }
    }
}
