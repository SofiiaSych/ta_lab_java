package com.epam.labs.oopAndGame.electricalAppliances.model;

import com.epam.labs.oopAndGame.electricalAppliances.data.ApplianceSize;
import com.epam.labs.oopAndGame.electricalAppliances.service.IShreddable;
import com.epam.labs.oopAndGame.electricalAppliances.appliance.KitchenAppliance;
import com.epam.labs.oopAndGame.electricalAppliances.data.ShreddingLevel;

import java.time.Duration;

public class Blender extends KitchenAppliance implements IShreddable {
    public Blender(int power, ApplianceSize size){
        super(power, size);
    }
    public void shred(ShreddingLevel targetLevel) {
        System.out.println("Begin shredding");
        switch (targetLevel){
            case ROUGH:
                Duration.ofSeconds(10);
                System.out.println("shredded to rough level");
                break;
            case MEDIUM:
                Duration.ofSeconds(15);
                System.out.println("shredded to medium level");
                break;
            case SMALL:
                Duration.ofSeconds(20);
                System.out.println("shredded to small level");
                break;
        }
    }
}
