package com.epam.labs.lambdaAndStreams.lambda.task2;

import com.epam.labs.lambdaAndStreams.lambda.task2.meatOperations.BoilMeatOperation;
import com.epam.labs.lambdaAndStreams.lambda.task2.meatOperations.RoastMeatOperation;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import java.util.Scanner;

public class Solution {
        private static final Logger logger = LogManager.getLogger(Solution.class);
    public static void main(String[] args) {
        logger.info("Enter operation name and press enter");
        String operationName = new Scanner(System.in).nextLine();
        KitchenOperationExecutor kitchenOperationExecutor = new KitchenOperationExecutor();
        logger.info("Choose one of the ways (lambda, reference, anonymous, object) and press enter");
        String option = new Scanner(System.in).nextLine();
        switch (option){
            case "lambda":
                logger.info(kitchenOperationExecutor.executeOperation((operation) ->
                        ((new Meat("pork", 360).smoking(operation))), operationName));
                break;
            case "reference":
                Meat meat = new Meat("lamb", 90);
                logger.info(kitchenOperationExecutor.executeOperation(meat::stewing, operationName));
            case "anonymous":
                logger.info(new KitchenOperationExecutor()
                        .executeOperation(new RoastMeatOperation(new Meat("beef", 20)), operationName));
            case "object":
                logger.info(kitchenOperationExecutor
                        .executeOperation(new BoilMeatOperation(new Meat("chicken", 60)), operationName));
            default:
                logger.info(kitchenOperationExecutor
                        .executeOperation(new BoilMeatOperation(new Meat("chicken", 60)), operationName));
                break;
        }
    }
}
