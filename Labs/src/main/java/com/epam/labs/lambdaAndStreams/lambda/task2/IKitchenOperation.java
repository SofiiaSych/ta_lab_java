package com.epam.labs.lambdaAndStreams.lambda.task2;

@FunctionalInterface
public interface IKitchenOperation {
    String cook(String operationName);
}
