package com.epam.labs.lambdaAndStreams.lambda.task2;

import java.util.ArrayList;
import java.util.List;

public class KitchenOperationExecutor {
    private final List<IKitchenOperation> kitchenOperationList = new ArrayList<>();

    public String executeOperation(IKitchenOperation kitchenOperation, String operationName) {
        this.kitchenOperationList.add(kitchenOperation);
        return kitchenOperation.cook(operationName);
    }
}
