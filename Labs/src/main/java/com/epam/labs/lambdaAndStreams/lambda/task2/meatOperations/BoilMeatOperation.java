package com.epam.labs.lambdaAndStreams.lambda.task2.meatOperations;

import com.epam.labs.lambdaAndStreams.lambda.task2.IKitchenOperation;
import com.epam.labs.lambdaAndStreams.lambda.task2.Meat;

public class BoilMeatOperation  implements IKitchenOperation {
    private Meat meat;

    public BoilMeatOperation(Meat meat) {
        this.meat = meat;
    }

    @Override
    public String cook(String operationName) {
        return meat.boiling(operationName);
    }
}
