package com.epam.labs.lambdaAndStreams.lambda.task2.meatOperations;

import com.epam.labs.lambdaAndStreams.lambda.task2.IKitchenOperation;
import com.epam.labs.lambdaAndStreams.lambda.task2.Meat;

public class RoastMeatOperation implements IKitchenOperation {
    private Meat meat;

    public RoastMeatOperation(Meat meat) {
        this.meat = meat;
    }

    @Override
    public String cook(String operationName) {
        return meat.roasting(operationName);
    }
}
