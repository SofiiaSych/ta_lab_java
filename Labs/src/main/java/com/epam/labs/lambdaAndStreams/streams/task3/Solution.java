package com.epam.labs.lambdaAndStreams.streams.task3;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Solution {
    private static final Logger logger = LogManager.getLogger(Solution.class);
    public static void main(String[] args) {
        logger.info("List created by Stream.generate()");
        List<Integer> generatedList = CreateListByGenerator();
        generatedList.forEach(el -> System.out.println(el));
        OperationsWithList(generatedList);

        logger.info("List created by Stream.iterate()");
        List<Integer> iteratedList = CreateListByIterator();
        iteratedList.forEach(el -> System.out.println(el));
        OperationsWithList(iteratedList);
    }

    private static List<Integer> CreateListByGenerator() {
        return Stream.generate(() -> new Random()
                .nextInt(100))
                .limit(20)
                .collect(Collectors.toList());
    }

    private static List<Integer> CreateListByIterator() {
        return Stream.iterate(0, n -> n + new Random().nextInt(20))
                .limit(20)
                .collect(Collectors.toList());
    }

    private static int CountGreatestThanAverage(List<Integer> entryList){
        if (entryList == null){
            logger.error("Entered bad list");
            return 0;
        }
        int average = (int) entryList.stream().mapToInt(Integer::byteValue).average().getAsDouble();
        return entryList.stream().filter(s -> s > average).mapToInt(Integer::byteValue).sum();
    }

    private static void OperationsWithList(List<Integer> entryList){
        if (entryList == null){
            logger.error("Entered bad list");
            return;
        }
        logger.info("Maximum of this list");
        logger.info(entryList.stream().max(Integer::compare));
        logger.info("Minimum of this list");
        logger.info(entryList.stream().min(Integer::compare));
        logger.info("Average of this list");
        logger.info(entryList.stream().mapToInt(Integer::intValue).average().getAsDouble());
        logger.info("Sum of this list elements with using sum()");
        logger.info(entryList.stream().mapToInt(Integer::intValue).sum());
        logger.info("Sum of this list elements with using reduce()");
        logger.info(entryList.stream().reduce((lhs,rhs)->lhs + rhs));
        logger.info("Count of numbers greatest than average");
        logger.info(CountGreatestThanAverage(entryList));
    }
}
