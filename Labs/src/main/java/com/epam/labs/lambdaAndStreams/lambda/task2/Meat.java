package com.epam.labs.lambdaAndStreams.lambda.task2;

public class Meat {
    private String name;
    private int time;

    public Meat(String name, int time) {
        this.name = name;
        this.time = time;
    }

    public String boiling(String description){
        return this.name  + " was " + description + " during " + this.time;
    }
    public String roasting(String description){
        return this.name  + " was " + description + " during " + this.time;
    }
    public String stewing(String description){
        return this.name  + " was " + description + " during " + this.time;
    }
    public String smoking(String description){
        return this.name  + " was " + description + " during " + this.time;
    }
}
