package com.epam.labs.lambdaAndStreams.streams.task4;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.*;
import java.util.stream.Collectors;

public class Solution {
    private static final Logger logger = LogManager.getLogger(Solution.class);
    public static void main(String[] args) {

        logger.info("Enter word");
        List<String> stringList = CreateList();

        logger.info("Your words");
        stringList.stream().forEach((el)-> logger.info(el));

        logger.info("Number of unique words:");
        logger.info(CountUniqueWords(stringList));

        logger.info("Sorted unique words:");
        logger.info(SortUniqueWords(stringList));

        logger.info("Count unique words");
        for (Map.Entry<String, Long> item : CountWordOccurrence(stringList).entrySet()){
            logger.info(item.getKey() + " " + item.getValue());
        }

        logger.info("Count unique characters");
        logger.info(CountCharacterOccurrence(stringList));
    }

    private static List<String> CreateList() {
        List<String> stringList = new ArrayList<>();
        String scanner;
        do {
            scanner = new Scanner(System.in).nextLine();
            if (!scanner.isBlank()) {
                stringList.add(scanner);
            }
        }
        while (!scanner.isBlank());
        return stringList;
    }

    private static List<String> GetUniqueWords(List<String> bunch){
        if (bunch == null){
            logger.info("Entered invalid list");
            return null;
        }
        if (bunch.isEmpty()){
            logger.info("Entered empty list");
            return null;
        }
        return bunch.stream().distinct().collect(Collectors.toList());
    }

    private static long CountUniqueWords(List<String> words){
        return GetUniqueWords(words).stream().count();
    }

    private static List<String> SortUniqueWords(List<String> words){
        return GetUniqueWords(words).stream().sorted().collect(Collectors.toList());
    }

    private static Map<String, Long> CountWordOccurrence(List<String> bunch){
        return bunch
                .stream()
                .collect(Collectors.groupingBy(String::toLowerCase, Collectors.counting()));
    }

    private static Map<Character, Long> CountCharacterOccurrence(List<String> bunch){
        List<Character> characters = toCharacterList(bunch);
        return  characters
                .stream()
                .filter(Character::isLowerCase)
                .collect(Collectors.groupingBy(Character::charValue, Collectors.counting()));
        }

    private static List<Character> toCharacterList(List<String> stringList){
        StringBuilder allItemsInOne = new StringBuilder();
        stringList.forEach(element -> allItemsInOne.append(element));

       return allItemsInOne
               .toString()
               .chars()
               .mapToObj(el -> (char)el)
               .collect(Collectors.toList());
    }
}
