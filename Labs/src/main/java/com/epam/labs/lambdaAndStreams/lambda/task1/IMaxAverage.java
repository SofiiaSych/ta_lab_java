package com.epam.labs.lambdaAndStreams.lambda.task1;

@FunctionalInterface
public interface IMaxAverage {
    int maxAvgValue (int a, int b, int c);
}
