package com.epam.labs.lambdaAndStreams.lambda.task1;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class Solution{
    private static final Logger logger = LogManager.getLogger(Solution.class);
    public static void main(String[] args) {
        IMaxAverage maxValue = (a, b, c) -> Math.max(a,Math.max(b,c));
        IMaxAverage avgValue = (a, b, c) -> (int)((a + b + c)/3);
        logger.info(maxValue.maxAvgValue(4,6, 8));
        logger.info(avgValue.maxAvgValue(4,6, 8));
    }
}
