package com.epam.labs.lambdaAndStreams.lambda.task2.meatOperations;

import com.epam.labs.lambdaAndStreams.lambda.task2.IKitchenOperation;
import com.epam.labs.lambdaAndStreams.lambda.task2.Meat;

public class SmokeMeatOperation implements IKitchenOperation {
    private Meat meat;

    public SmokeMeatOperation(Meat meat) {
        this.meat = meat;
    }

    @Override
    public String cook(String operationName) {
        return meat.smoking(operationName);
    }
}
