package com.epam.labs.arrays.logicTaskD;

public interface Quest {
    void impact (Hero hero);
    @Override
    String toString();
}
