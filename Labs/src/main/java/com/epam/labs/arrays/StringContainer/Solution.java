package com.epam.labs.arrays.stringContainer;

import com.google.common.base.Stopwatch;

import java.util.ArrayList;
import java.util.List;

public class Solution {
    public static void main(String[] args) {
        Stopwatch stopwatch = Stopwatch.createStarted();
        StringContainer strArrayContainer = new StringContainer();
        System.out.println(strArrayContainer);
        for (int i = 0; i < 26; i++) {
            strArrayContainer.putStringIntoArray(i,"String №" + i+1);
            System.out.println(strArrayContainer.toString());
        }
        stopwatch.stop();
        System.out.println("Performance of custom string array container: " + stopwatch);
        stopwatch.reset().start();

        List<String> arrayList = new ArrayList<String>();
        for (int i = 0; i < 26; i++) {
            arrayList.add("String №" + i+1);
            System.out.println(arrayList);
        }
        stopwatch.stop();
        System.out.println("Performance of ArrayList: " + stopwatch);
    }
}
