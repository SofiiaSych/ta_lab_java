package com.epam.labs.arrays.stringComparator;

import java.util.Comparator;

public class StringComparator implements Comparator<StringComparator> {
    private String firstName;
    private String lastName;

    public StringComparator(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getName() {
        return firstName;
    }
    public String getSurname() {
        return lastName;
    }

    @Override
    public int compare(StringComparator o1, StringComparator o2) {
        return o1.firstName.compareTo(o2.firstName);
    }

    @Override
    public String toString() {
        return "First name: " + firstName + "\nLast name:  " + lastName;
    }
}
