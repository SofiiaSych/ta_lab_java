package com.epam.labs.arrays.deque;

public interface IMyDeque {
    void addFirst(Object newItem);

    void addLast(Object newItem);

    Object getFirst();

    Object getLast();

    void removeFirst();

    void removeLast();

    int size();
}
