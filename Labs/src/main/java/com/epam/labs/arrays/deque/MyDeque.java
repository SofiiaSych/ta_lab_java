package com.epam.labs.arrays.deque;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.Arrays;

public class MyDeque implements IMyDeque {
    private static final Logger logger = LogManager.getLogger(MyDeque.class);
    int MIN_SIZE = 16;
    Object[] deque = new Object[MIN_SIZE];
    int lastItem;

    public MyDeque(Object[] deque) {

        this.deque = deque;
        this.lastItem = deque.length - 1;
    }

    @Override
    public void addFirst(Object newItem) {
        if (newItem == null){
            logger.error("Null element");
            return;
        }
        Object[] newArray = new Object[this.deque.length + 1];
        System.arraycopy(this.deque, 0, newArray, 1, this.deque.length);
        newArray[0] = newItem;
        this.deque = newArray;
        this.lastItem++;
    }

    @Override
    public void addLast(Object newItem) {
        if (newItem == null){
            logger.error("Null element");
           return;
        }
        Object[] newArray = new Object[this.deque.length + 1];
        System.arraycopy(this.deque, 0, newArray, 0, this.deque.length);
        this.lastItem++;
        newArray[lastItem] = newItem;
        this.deque = newArray;

    }

    @Override
    public Object getFirst() {
        return this.deque[0];
    }

    @Override
    public Object getLast() {
        return this.deque[this.lastItem];
    }

    @Override
    public void removeFirst() {
        Object[] newArray = new Object[this.deque.length - 1];
        System.arraycopy(this.deque, 1, newArray, 0, this.deque.length - 1);
        this.deque = newArray;
        this.lastItem--;
    }

    @Override
    public void removeLast() {
        Object[] newArray = new Object[this.deque.length - 1];
        System.arraycopy(this.deque, 0, newArray, 0, this.deque.length - 1);
        this.deque = newArray;
        this.lastItem--;
    }

    @Override
    public int size() {
        return this.deque.length;
    }

    @Override
    public String toString() {
        return "Objects" + Arrays.toString(this.deque);
    }
}
