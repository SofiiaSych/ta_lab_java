package com.epam.labs.arrays.stringComparator;

import java.util.*;

public class Solution {
    private static Comparator<StringComparator> comparator = Comparator.comparing(lastName->lastName.getSurname());

    public static void main(String[] args) {
        StringComparator person1 = new StringComparator("David", "Schwimmer");
        StringComparator person2 = new StringComparator("Jennifer", "Aniston");
        StringComparator person3 = new StringComparator("Lisa", "Kudrow");
        StringComparator person4 = new StringComparator("Matt", "LeBlanc");
        StringComparator person5 = new StringComparator("Matthew", "Perry");
        StringComparator person6 = new StringComparator("Courteney", "Cox");

        StringComparator[] friendsArray = {person1, person2, person3, person4, person5, person6};

        System.out.println("Unsorted array:");
        for (int i = 0; i < friendsArray.length; i++){
            System.out.println(friendsArray[i]);
        }

        Arrays.sort(friendsArray,comparator);

        System.out.println("Sorted array:");
        for (int i = 0; i < friendsArray.length; i++){
            System.out.println(friendsArray[i]);
        }

        List<StringComparator> friendsList = new ArrayList<>(Arrays.asList(person1, person2, person3, person4, person5, person6));

        System.out.println("Unsorted list:");
        for (StringComparator person : friendsList) {
            System.out.println(person);
        }

        System.out.println("Sorted list:");
        Collections.sort(friendsList,comparator);
        for (StringComparator person : friendsList) {
            System.out.println(person);
        }

        System.out.println("Binary search:");
        int item = Collections.binarySearch(friendsList, person3, comparator);
        System.out.println(item);
    }
}
