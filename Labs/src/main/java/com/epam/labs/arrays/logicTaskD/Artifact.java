package com.epam.labs.arrays.logicTaskD;

public class Artifact implements Quest {
    int strength;

    public Artifact (int strength){
        if (strength > 80){
            this.strength = 80;
        }
        else if(strength < 10){
            this.strength = 10;
        }
        else {
            this.strength = strength;
        }
    }
    public void impact(Hero hero) {
        hero.strength = hero.strength + this.strength;
        System.out.println("Hero earns " + this.strength + " points.");
    }

    @Override
    public String toString() {
        return "This artifact has strength " + this.strength;
    }
}
