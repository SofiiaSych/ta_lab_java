package com.epam.labs.arrays.logicTaskD;

public abstract class Character {
    String name;
    int strength;

    public Character(){
    }

    public Character(String name, int strength){
        this.name = name;
        this.strength = strength;
    }
}
