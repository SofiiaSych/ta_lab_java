package com.epam.labs.arrays.logicTaskD;

public class Monster  extends Character implements Quest{
    public Monster (String name, int strength){
        super(name, strength);
        if (strength > 100){
            this.strength = 100;
        }
        else if(strength < 5) {
            this.strength = 5;
        }
        else {
            this.strength = strength;
        }
    }

    public void impact(Hero hero) {
        hero.strength = hero.strength - this.strength;
        System.out.println("Hero has encountered with monster and lost " + this.strength + " of strength.");
    }

    @Override
    public String toString() {
        return "I'm monster " + this.name + " with strength " + this.strength;
    }
}
