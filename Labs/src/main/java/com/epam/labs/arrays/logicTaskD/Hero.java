package com.epam.labs.arrays.logicTaskD;

public class Hero extends Character{
    public Hero (String name, int strength){super(name, strength);}
    public Hero(){
        this.name = "Mario";
        this.strength = 25;
    };

        @Override
    public String toString() {
        return "I'm hero " + this.name + " with strength " + this.strength;
    }
}
