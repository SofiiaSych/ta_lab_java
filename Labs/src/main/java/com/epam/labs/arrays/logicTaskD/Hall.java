package com.epam.labs.arrays.logicTaskD;

import java.util.Random;

public class Hall {
    public Door[] doors;
    private int numAllDoors = 10;
    private int numArtifactDoors = 0;
    private int numMonsterDoors = 0;
    public Hall() {
        this.doors = new Door[numAllDoors];
        for (int i = 0; i < this.doors.length; i++){
            int option = new Random().nextInt(2);
            switch (option) {
                case 0:
                    Artifact artifact = new Artifact(new Random().nextInt(85));
                    this.doors[i] = new Door(i + 1 , artifact);
                    this.numArtifactDoors++;
                    System.out.println("Door num " + this.doors[i].num + ": " + this.doors[i].quest.toString());
                    break;
                case 1:
                    Monster monster = new Monster("monster" + i, new Random().nextInt(105));
                    this.doors[i] = new Door(i + 1, monster);
                    this.numMonsterDoors++;
                    System.out.println("Door num " + this.doors[i].num + ": " + this.doors[i].quest.toString());
                    break;
            }
        }
    }

    public boolean IsSurvivable(Hero hero){
        int strengthCounter = 0;

        for(Artifact artifact : CollectAllArtifacts()){
            strengthCounter = strengthCounter + artifact.strength;
        }

        for(Monster monster : FaceAllMonsters()){
            strengthCounter = strengthCounter - monster.strength;
        }

        if (strengthCounter < 0)
            return false;
        else
            return true;
    }

    public int CountDeadDoors(Hero hero){
        int doorCounter = 0;
        for(Monster monster : this.FaceAllMonsters()){
            if(monster.strength >= hero.strength){
                doorCounter++;
            }
        }
        return doorCounter;
    }

    public void QuestHall(Hero hero){
        this.OpenAllArtifactDoors(hero);
        this.OpenAllMonsterDoors(hero);
    }

    private Door[] GetArtifactDoors(){
        Door[] artifactDoors = new Door[this.numArtifactDoors];
        int counter = 0;
        for (Door door : this.doors) {
            if (door.quest instanceof Artifact) {
                artifactDoors[counter] = door;
                counter++;
            }
        }
        return artifactDoors;
    }

    private Door[] GetMonsterDoors(){
        Door[] monsterDoors = new Door[this.numMonsterDoors];
        int counter = 0;
        for (Door door : this.doors) {
            if (door.quest instanceof Monster) {
                monsterDoors[counter] = door;
                counter++;
            }
        }
        return monsterDoors;
    }

    private Artifact[] CollectAllArtifacts() {
        Artifact[] artifacts = new Artifact[this.numArtifactDoors];
        Door[] artifactDoors = this.GetArtifactDoors();
        int counter = 0;
        for (Door door : artifactDoors) {
            artifacts[counter] = ((Artifact) door.quest);
            counter++;
        }
        return artifacts;
    }

    private Monster[] FaceAllMonsters(){
        Monster[] monsters = new Monster[this.numMonsterDoors];
        Door[] monsterDoors = this.GetMonsterDoors();
        int counter = 0;
        for (Door door : monsterDoors) {
             monsters[counter] = ((Monster) door.quest);
             counter++;
        }
        return monsters;
    }

    private void OpenAllArtifactDoors(Hero hero){
        Door[] artifactDoors = this.GetArtifactDoors();
        System.out.println("Collecting artifacts");
        for (Door door : artifactDoors){
            System.out.println("Opening door " + door.num);
            door.quest.impact(hero);
        }
        System.out.println("Now i have " + hero.strength + " of strength and ready to face monsters");
    }

    private void OpenAllMonsterDoors(Hero hero){
        Door[] monsterDoors = this.GetMonsterDoors();
        System.out.println("Facing monsters");
        for(Door door: monsterDoors){
            System.out.println("Opening door " + door.num);
            door.quest.impact(hero);
        }
        System.out.println("After facing monster I have " + hero.strength + " left.");
    }
}
