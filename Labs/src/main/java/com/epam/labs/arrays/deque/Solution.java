package com.epam.labs.arrays.deque;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class Solution {
    private static final Logger logger = LogManager.getLogger(MyDeque.class);

    public static void main(String[] args){
        String[] array = new String[16];
        for(Integer i = 0; i < array.length; i++){
            array[i] = "object " + (i + 1);
        }

        MyDeque newDeque = new MyDeque(array);
        logger.info(newDeque);

        newDeque.addFirst("lalaland");
        logger.info(newDeque.toString());

        newDeque.addLast("interstellar");
        logger.info(newDeque.toString());

        newDeque.removeFirst();
        logger.info(newDeque.toString());

        newDeque.removeLast();
        logger.info(newDeque.toString());

        logger.info("First object " + newDeque.getFirst());
        logger.info("Last object " + newDeque.getLast());
    }
}
