package com.epam.labs.arrays.logicTaskC;
import java.util.Arrays;

public class Solution {
    public static void main(String[] args){
        System.out.println("Initial array");
        Integer[] introArray = new Integer[] {1, 2, 3, 3, 3, 6, 5, 3, 7, 6, 6, 6, 6, 10, 9, 6, 11, 11, 11, 11, 3, 11, 5};
        System.out.println(Arrays.toString(introArray));
        Integer[] cleanedArray = CleanArray(introArray);
        System.out.println("Cleaned array");
        System.out.println(Arrays.toString(cleanedArray));

    }

    private static void NullRepeatedElement(Integer[] array){
        for (int i=0; i < array.length - 1; i++){
            if (array[i] == array[i+1]){
                array[i] = null;
            }
        }
    }

    private static int GetLengthWithoutNulls(Integer[] array){
        int counter = 0;
        for(Integer item : array){
            if (item != null)
                counter++;
        }
        return counter;
    }

    public static Integer[] CleanArray(Integer[] array){
        int counter = 0;
        NullRepeatedElement(array);
        Integer[] cleanedArray = new Integer[GetLengthWithoutNulls(array)];
        for(Integer item: array){
            if (item != null){
                cleanedArray[counter] = item;
                counter++;
            }
        }
        return cleanedArray;
    }

}
