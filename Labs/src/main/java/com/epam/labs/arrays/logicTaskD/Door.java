package com.epam.labs.arrays.logicTaskD;

public class Door {
    public int num;
    public Quest quest;

    public Door(int num, Quest quest){
        this.num = num;
        this.quest = quest;
    }

    public void open(Hero hero){
        quest.impact(hero);
    }

}
