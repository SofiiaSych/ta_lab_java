package com.epam.labs.arrays.logicTaskA;

import java.util.Arrays;

public class Solution {
    public static void main(String[] args){
        int[] lhsArray = new int[] {1,2,3,4,5,6,7,8,9,10};
        int[] rhsArray = new int[] {5,6,7,8,9,10,11,12,13,14,15};

        System.out.println("Num of duplicates");
        System.out.println(FindNumOfDuplicates(lhsArray, rhsArray));
        System.out.println("Duplicates");
        System.out.println(Arrays.toString(CreateDuplicatesArray(lhsArray, rhsArray)));
        System.out.println("Num of distinct values");
        System.out.println(FindNumOfDistinct(lhsArray, rhsArray));
        System.out.println("Distinct");
        System.out.println(Arrays.toString(CreateDistinctArray(lhsArray, rhsArray)));
    }

    private static int FindNumOfDuplicates(int[] lhs, int[]rhs){
        int counter = 0;
        for(int i: lhs){
            for (int j:rhs){
                if (i==j) counter ++;
            }
        }
        return counter;
    }

    private static int FindNumOfDistinct(int[] lhs, int[]rhs){
        int counter = 0;
        boolean duplicateFound = false;
        for(int i: lhs){
            for (int j: rhs){
                if (i==j) duplicateFound = true;
            }
            if (!duplicateFound) counter++;
            duplicateFound = false;
        }
        for(int i: rhs){
            for (int j: lhs){
                if (i==j) duplicateFound = true;
            }
            if (!duplicateFound) counter++;
            duplicateFound = false;
        }
        return counter;
    }

    private static int[] CreateDuplicatesArray(int[] lhs, int[]rhs){
        int counter = 0;
        int duplicateLength = FindNumOfDuplicates(lhs, rhs);
        int[] duplicates = new int[duplicateLength];
        for(int i: lhs){
            for (int j:rhs){
                if (i==j) {
                    duplicates[counter] = i;
                    counter ++;
                }
            }
        }
        return duplicates;
    }

    private static int[] CreateDistinctArray(int[] lhs, int[]rhs){
        int counter = 0;
        boolean duplicateFound = false;
        int distinctLength = FindNumOfDistinct(lhs, rhs);
        int[] distinct = new int[distinctLength];

        for(int i: lhs){
            for (int j: rhs){
                if (i==j) duplicateFound = true;
            }
            if (!duplicateFound) {
                distinct[counter] = i;
                counter++;
            }
            duplicateFound = false;
        }

        for(int i: rhs){
            for (int j: lhs){
                if (i==j) duplicateFound = true;
            }
            if (!duplicateFound) {
                distinct[counter] = i;
                counter++;
            }
            duplicateFound = false;
        }

        return distinct;
    }
}
