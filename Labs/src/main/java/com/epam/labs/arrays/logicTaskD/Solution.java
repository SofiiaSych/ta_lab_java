package com.epam.labs.arrays.logicTaskD;

public class Solution {
    public static void main(String[] args){
        Hero hero = new Hero();
        Hall hall = new Hall();

        System.out.println(hero.toString());

        System.out.println("Dead is waiting behind " + hall.CountDeadDoors(hero) + " doors.");

        if (!hall.IsSurvivable(hero)){
            System.out.println("it's impossible to win this game.");
            return;
        }

        hall.QuestHall(hero);


        //System.out.println("Hero strength is " + hero.strength);
    }
}
