package com.epam.labs.arrays.logicTaskB;

import java.util.Arrays;

public class Solution {
    public static void main(String[] args){
        // Array with duplicate elements
        int[] originArray = new int[] {1, 2, 3, 4, 5, 6, 5, 7, 6, 8, 10, 8, 10, 9, 11, 10, 12, 1, 3, 5};
        int[] output = Arrays.stream(originArray).distinct().toArray();

        System.out.println(Arrays.toString(output));
    }
}
