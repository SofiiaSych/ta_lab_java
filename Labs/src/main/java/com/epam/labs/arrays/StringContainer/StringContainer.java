package com.epam.labs.arrays.stringContainer;

import java.util.Arrays;

public class StringContainer {
    private String [] strArrayContainer;

    public StringContainer() {
        this.strArrayContainer = new String[25];
    }

    public String[] takeCopyOfArray() {
        return strArrayContainer.clone();
    }

    public void putStringIntoArray(int index, String str) {
        if(index < strArrayContainer.length)
            this.strArrayContainer[index] = str;
        else {
            this.strArrayContainer = increaseArray();
            this.strArrayContainer[index] = str;
        }
    }

    private String[] increaseArray() {
        int newSizeOfArray = strArrayContainer.length + 1;
        String[] sourceArray;
        sourceArray = strArrayContainer;
        String[] destinationArray;
        destinationArray = new String[newSizeOfArray];
        System.arraycopy(sourceArray, 0, destinationArray, 0, strArrayContainer.length);
        return destinationArray;
    }

    @Override
    public String toString() {
        return "Container:" + Arrays.toString(strArrayContainer);
    }
}
