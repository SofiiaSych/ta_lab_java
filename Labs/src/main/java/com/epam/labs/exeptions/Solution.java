package com.epam.labs.exeptions;

import java.util.Scanner;

public class Solution {

    public static void main(String[] args){
        System.out.println("Enter first side");
        int sideA = new Scanner(System. in).nextInt();
        System.out.println("Enter second side");
        int sideB = new Scanner(System. in).nextInt();

        try{
            System.out.println("Rectangular square = " + squareRectangular(sideA, sideB));
        }
        catch (LessThanZeroException ltzex){
            System.out.println(ltzex.getMessage());
        }
        Plant nezabutka = new Plant(20, Color.BLUE, Type.GRASS);
        Plant appleTree = new Plant(20, Color.GREEN, Type.TREE);
        Plant cherryTree = new Plant(320, Color.GREEN, Type.TREE);
        Plant raspberry = new Plant(120, Color.BLUE, Type.BUSH);
        Plant currant = new Plant(150, Color.GREEN, Type.BUSH);

        Plant[] garden = new Plant[]{nezabutka, appleTree, cherryTree, raspberry, currant};

        for (int i = 0; i < garden.length; i++){
            try {
                System.out.println(garden[i].ToString());
            }
            catch (TypeException typeException){
                System.out.println(typeException.getMessage());
            }
            catch (ColorException colorException){
                System.out.println(colorException.getMessage());
            }
        }
    }

    static int squareRectangular(int a, int b) throws LessThanZeroException{
        if (a < 0 || b < 0) throw new LessThanZeroException("One of numbers is less than zero");
        return a * b;
    }
}

