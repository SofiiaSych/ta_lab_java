package com.epam.labs.exeptions;

class LessThanZeroException extends Exception{
    public LessThanZeroException(String message){
        super(message);
    }
}
