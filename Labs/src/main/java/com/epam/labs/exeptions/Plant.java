package com.epam.labs.exeptions;

public class Plant {
    private int size;
    private Color color;
    private Type type;
    public Plant(int size, Color color, Type type){
        this.size = size;
        this.color = color;
        this.type = type;
    }
    public String ToString() throws ColorException, TypeException{
        if (this.type == Type.TREE && this.size < 100 ) throw  new TypeException("Tree can't have such small size");
        if (this.type == Type.BUSH && this.color == Color.BLUE) throw new ColorException("Bush can't have this color");
        return "Plant with size " + this.size + " has " + this.color + " and belongs to " + this.type + " type";

    }
}
