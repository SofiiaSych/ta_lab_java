package com.epam.labs.exeptions;

public enum Type {
    TREE,
    BUSH,
    GRASS
}
