package com.epam.labs.exeptions;

public class ColorException extends Exception {
    public ColorException(String message){
        super(message);
    }
}
