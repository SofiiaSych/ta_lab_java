package com.epam.labs.exeptions;

public class TypeException extends Exception {
    public TypeException(String message){
        super(message);
    }
}
