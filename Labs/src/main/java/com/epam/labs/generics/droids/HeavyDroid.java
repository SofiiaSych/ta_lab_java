package com.epam.labs.generics.droids;

public class HeavyDroid extends Droid {
    private int weight;

    public HeavyDroid(int id, String name, int weight) {
        super(id, name);
        this.weight = weight;
    }
    @Override
    public String toString() {
        return "I`m a heavy droid with id " + this.id + ", name " + this.name + ", weight " + this.weight + "\n";
    }
}
