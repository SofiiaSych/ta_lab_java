package com.epam.labs.generics.priorityQueue;

import java.util.Comparator;
import java.util.Iterator;

public interface IMyPriorityQueue<E extends Object & Comparable<E>> {
    boolean	add(E e);
    void clear();
    boolean	contains(Object o);
    Iterator<E> iterator();
    boolean	offer(E e);
    E peek();
    E poll();
    boolean	remove(Object o);
    int	size();
}
