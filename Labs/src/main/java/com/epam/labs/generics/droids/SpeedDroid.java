package com.epam.labs.generics.droids;

public class SpeedDroid extends Droid {
    private int speed;

    public SpeedDroid(int id, String name, int speed) {
        super(id, name);
        this.speed = speed;
    }
    @Override
    public String toString() {
        return "I`m a speed droid with id " + this.id + ", name " + this.name + ", speed " + this.speed + "\n";
    }
}
