package com.epam.labs.generics.droids;

public class Droid implements Comparable<Droid> {
    protected int id;
    protected String name;

    public Droid(int id, String name){
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "I`m a droid with id " + this.id + ", name " + this.name + "\n";
    }

    @Override
    public int compareTo(Droid o) {
        return this.getName().compareTo(o.getName());
    }
}
