package com.epam.labs.generics.addStringToIntList;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class Solution {
    private static final Logger logger = LogManager.getLogger(Solution.class);

    public static void main(String[] args) {
        List<Integer> integers = new ArrayList<>();
        addParsedString(integers, "22");
        addParsedString(integers, "33");
        integers.add(44);

        for(Integer integer: integers){
            logger.info(integer);
        }
    }

    public static void addParsedString(List list, String strToAdd){
        try
        {
            int item = Integer.parseInt(strToAdd.trim());
            list.add(item);
            logger.info("Added parsed number " + item);
        }
        catch (NumberFormatException nfe)
        {
            logger.error("NumberFormatException: " + nfe.getMessage());
        }
    }
}
