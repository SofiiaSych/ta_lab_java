package com.epam.labs.generics.droids;

import java.util.ArrayList;
import java.util.List;

public class Solution {
    public static void main(String args[]){
        Ship<Droid> droidShip = new Ship();
        droidShip.putDroid(new Droid(1, "Droid1"));
        droidShip.putDroid((new Droid(2, "Droid2")));
        droidShip.putDroid((new Droid(3, "Droid3")));

        List<HeavyDroid> heavyDroidList = new ArrayList<>();
        heavyDroidList.add(new HeavyDroid(1, "HeavyDroid1", 2000));
        heavyDroidList.add(new HeavyDroid(2, "HeavyDroid2", 5000));
        heavyDroidList.add(new HeavyDroid(3, "HeavyDroid3", 3500));

        List<SpeedDroid> speedDroidList = new ArrayList<>();
        speedDroidList.add(new SpeedDroid(1, "SpeedDroid1", 750));
        speedDroidList.add(new SpeedDroid(2, "SpeedDroid2", 520));
        speedDroidList.add(new SpeedDroid(3, "SpeedDroid3", 630));

        droidShip.putAllDroids(heavyDroidList);
        droidShip.putAllDroids(speedDroidList);
        System.out.println(droidShip.toString());
    }
}
