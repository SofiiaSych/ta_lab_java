package com.epam.labs.generics.priorityQueue;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.AbstractQueue;
import java.util.Arrays;
import java.util.Iterator;

public class MyPriorityQueue<E extends Object & Comparable<E>>
        extends AbstractQueue implements IMyPriorityQueue {
    private static final Logger logger = LogManager.getLogger(MyPriorityQueue.class);
    private E [] array;
    private int lastElement;

    public MyPriorityQueue() {
        this.array = (E[]) new Object[0];
        lastElement = this.array.length - 1;
    }

    @Override
    public boolean add(Object itemToAdd) {
        return this.offer(itemToAdd);
    }

    @Override
    public void clear() {
        this.array  = (E[]) new Object[0];
    }

    @Override
    public boolean contains(Object itemToFind) {
        for(Object object: this.array){
            if (itemToFind == object){
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public boolean offer(Object itemToAdd) {
        if (itemToAdd == null){
            return false;
        }
        try {
            E[] tempArray = (E[]) new Object[this.array.length + 1];
            System.arraycopy(this.array, 0, tempArray, 0, this.array.length);
            this.lastElement++;
            tempArray[lastElement] = (E) itemToAdd;
            Arrays.sort(tempArray);
            this.array = tempArray;
            logger.info("Method \"offer()\" added object: " + itemToAdd);
            return true;
        }catch (IndexOutOfBoundsException e){
            logger.error("Method \"offer()\" has failed to add object: " + itemToAdd + ": " + e.getLocalizedMessage());
            return false;
        }
    }

    @Override
    public Object peek() {
        if (this.array.length < 1){
            logger.error("Priority Queue is empty");
            return null;
        }
        return this.array[0];
    }

    @Override
    public Object poll() {
        Object pickedObject = peek();
        E[] tempArray = (E []) new Object[this.array.length - 1];
        System.arraycopy(this.array, 1, tempArray, 0, this.array.length - 1);
        this.array = tempArray;
        this.lastElement--;
        return pickedObject;
    }

    @Override
    public boolean remove(Object itemToRemove) {
        return super.remove(itemToRemove);
    }

    @Override
    public int size() {
        return this.array.length;
    }

    @Override
    public String toString() {
        return Arrays.toString(this.array);
    }
}
