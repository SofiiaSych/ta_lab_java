package com.epam.labs.generics.priorityQueue;

import com.epam.labs.generics.droids.Droid;
import com.epam.labs.generics.droids.HeavyDroid;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class Solution {
    private static final Logger logger = LogManager.getLogger(MyPriorityQueue.class);

    public static void main(String[] args){
        MyPriorityQueue<Droid> droids = new MyPriorityQueue<Droid>();
        Droid tars = new Droid(1, "Tars");

        droids.offer(tars);
        droids.offer(new Droid(2, "Case"));
        droids.offer(new HeavyDroid(3, "HeavyDroid2000", 2000));
        droids.offer(new HeavyDroid(4, "HeavyDroid3000", 3000));
        logger.info("Droids: " + droids.toString());

        logger.info("Picked droid is " + droids.peek().toString());
        logger.info("After peek");
        logger.info("Droids: " + droids.toString());

        logger.info("Polled droid is " + droids.poll().toString());
        logger.info("After poll");
        logger.info("Droids: " + droids.toString());

        logger.info("Do droids contain TARS? " + droids.contains(tars));

    }
}
