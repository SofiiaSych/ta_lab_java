package com.epam.labs.generics.droids;

import java.util.ArrayList;
import java.util.List;

public class Ship<T> {
    private List<T> listOfDroids;

    public Ship(){
        this.listOfDroids = new ArrayList<>();
    }

    public void putDroid(T droid){
        this.listOfDroids.add(droid);
    }
    public List<T> getDroid(){
        return listOfDroids;
    }
    public void putAllDroids(List <? extends T> list){
        for(T droid : list){
            listOfDroids.add(droid);
        }
    }
    @Override
    public String toString() {
        String aboutString = "This list contains next  droids: \n";
        for(T droid : this.listOfDroids)
            aboutString = aboutString + droid.toString();
        return  aboutString;
    }
}
