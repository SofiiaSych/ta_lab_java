package com.epam.labs.collectionsPart2.BinaryTreeMap;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

public class MyBinaryTreeMap<K, V> implements IMyBinaryTreeMap {
    private static final Logger logger = LogManager.getLogger(MyBinaryTreeMap.class);
    private Set<K> keySet;
    private V[] valueArray;
    private int lastElement;

    public MyBinaryTreeMap() {
        this.keySet = new TreeSet<>();
        this.valueArray = (V[]) new Object[0];
//        for (int i = 0; i < this.valueArray.length ; i++) {
//            this.valueArray[i] = (V) ("Value is " + i);
//        }
        this.lastElement = this.valueArray.length - 1;
    }

    @Override
    public Object get(Object key) {
        int value = getKeyPosition((K)key);
        return valueArray[value];
    }

    @Override
    public V put(Object key, Object value) {
        if (key == null) {
            return null;
        }
        keySet.add((K)key);
        addToValueArray(getKeyPosition((K)key),(V)value);
        return (V) value;
    }

    @Override
    public Object remove(Object key) {
        return null;
    }

    @Override
    public Object replace(Object key, Object value) {
        return null;
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public Collection values() {
        return null;
    }

    @Override
    public Set keySet() {
        return null;
    }

    @Override
    public boolean containsKey(Object key) {
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public String print() {
        String str = "";
        for (K key: keySet) {
            str = str + "\n" + key + " - " + get(key);
        }
        return str;
    }

    private void addToValueArray(int index, V value) {
        if (index < 0) {
            throw new IndexOutOfBoundsException();
        }
        try {
            V[] tempArray = (V[]) new Object[this.valueArray.length + 1];
            System.arraycopy(this.valueArray, 0, tempArray, 0, index);
            this.lastElement++;
            tempArray[index] = (V) value;
            int secondPartOfArrayLength = this.valueArray.length - index;
            System.arraycopy(this.valueArray, index, tempArray, index + 1, secondPartOfArrayLength);
            this.valueArray = tempArray;
            logger.info("Method \"addToValueArray()\" added object: " + value);
        } catch (IndexOutOfBoundsException e) {
            logger.error("Method \"addToValueArray()\" has failed to add object: " + value + ": " + e.getLocalizedMessage());
        }
    }
    private int getKeyPosition(K key) {
        int position = 0;
        if (position > keySet.size()) {
            position--;
        }
        for (K k : keySet) {
            if (k.equals(key)) {
                break;
            }
            position++;
        }
        return position;
    }
}
