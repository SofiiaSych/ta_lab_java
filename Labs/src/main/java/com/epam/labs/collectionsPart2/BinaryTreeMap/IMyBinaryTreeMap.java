package com.epam.labs.collectionsPart2.BinaryTreeMap;

import java.util.Collection;
import java.util.Set;

public interface IMyBinaryTreeMap<K, V> {
    V get(Object key);
    V put(K key, V value);
    V remove(Object key);
    V replace(K key, V value);
    int	size();
    Collection<V> values();
    Set<K> keySet();
    boolean containsKey(Object key);
    boolean	containsValue(Object value);
    void clear();
    String print();
}
