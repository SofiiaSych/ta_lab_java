package com.epam.labs.collectionsPart2.consoleWithEnumAndHashMap;

public enum CountryEnum {
    NORTH(0),
    EAST(1),
    SOUTH(2),
    WEST(3);

    private int value;

    private CountryEnum(int value){
        this.value = value;
    }
}
