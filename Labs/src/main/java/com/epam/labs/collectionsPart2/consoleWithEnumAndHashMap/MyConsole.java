package com.epam.labs.collectionsPart2.consoleWithEnumAndHashMap;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import java.util.HashMap;
import java.util.Scanner;

public class MyConsole {
    private static final Logger log = LogManager.getLogger(MyConsole.class);
    private final Scanner sc = new Scanner(System.in);
    private HashMap<CountryEnum, String> map;
    private Integer menuItem;

    public MyConsole() {
        this.map = new HashMap<CountryEnum, String>();
        map.put(CountryEnum.NORTH, "Norway");
        map.put(CountryEnum.EAST, "China");
        map.put(CountryEnum.SOUTH, "Antarctica");
        map.put(CountryEnum.WEST, "USA");
    }

    public void showMenu(){
        log.info("MENU");
        int iterator = 0;
        for (CountryEnum country: CountryEnum.values()){
            log.info(iterator + ". " + country);
            iterator++;
        }
    }

    public void interact(){
        log.info("Please select direction:");
        showMenu();
        log.info("Enter digit in range 0..3:");
        menuItem = sc.nextInt();
        CountryEnum side = CountryEnum.values()[menuItem];
        String country = this.map.get(side);
        System.out.println("You're heading to " + country);
    }
}
