package com.epam.labs.collectionsPart2.BinaryTreeMap;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class Solution {
    private static final Logger logger = LogManager.getLogger(Solution.class);

    public static void main(String[] args) {
        MyBinaryTreeMap<String, String> treeMap = new MyBinaryTreeMap<>();
        treeMap.put("Ukraine", "Kyiv");
        treeMap.put("Poland", "Warshawa");
        treeMap.put("Italy", "Rome");

        logger.info(treeMap.print());
    }
}
